package restremote.lese.com.restremote;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import restremote.lese.com.restremote.domain.Command;
import restremote.lese.com.restremote.domain.CustomCommand;
import restremote.lese.com.restremote.domain.DeviceApiService;
import restremote.lese.com.restremote.domain.DeviceRepo;


public class DeviceFragment extends SubscriberFragment {
	/**
	 * The fragment argument representing the section number for this
	 * fragment.
	 */
	private static final String ARG_DEVICE = "section_device";

	@InjectView(R.id.commandsList)
	protected ExpandableListView mCommandsList;

	@InjectView(R.id.addCustomButton)
	protected FloatingActionButton mAddButton;

	private CommandListAdapter mCommandsAdapter;
	private String mDeviceName;

	@Inject protected DeviceApiService mService;
	@Inject protected DeviceRepo mRepo;

	/**
	 * Returns a new instance of this fragment for the given section
	 * number.
	 */
	public static DeviceFragment newInstance(String deviceName) {
		DeviceFragment fragment = new DeviceFragment();
		Bundle args = new Bundle();
		args.putString(ARG_DEVICE, deviceName);
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_device, container, false);
		ButterKnife.inject(this, rootView);

		mDeviceName = getArguments().getString(ARG_DEVICE);

		mCommandsAdapter = new CommandListAdapter(getActivity());
		mCommandsList.setAdapter(mCommandsAdapter);

		StaticUtils.observe(this, mRepo.getCustomCommands())
				.subscribe(commands -> {
					mCommandsAdapter.setCustomCommands(commands);
					mCommandsAdapter.notifyDataSetChanged();
				});

		mRepo.load(mDeviceName);

		mCommandsList.setOnChildClickListener((p, v, group, child, id) -> {
			if (group == CommandListAdapter.GROUP_COMMANDS) {
				executeCommand(mCommandsAdapter.getCommand(child));
			} else {
				executeCommand(CustomCommand.toCommand(mCommandsAdapter.getCustomCommand(child)));
			}
			return true;
		});

		mCommandsList.setOnGroupCollapseListener(pos -> {
			if (pos == CommandListAdapter.GROUP_CUSTOM) {
				mAddButton.hide();
			}
		});

		mCommandsList.setOnGroupExpandListener(pos -> {
			if (pos == CommandListAdapter.GROUP_CUSTOM) {
				mAddButton.show();
			}
		});

		mAddButton.hide(false);

		if (mDeviceName != null && mService != null) {
			refreshCommands();
		}

		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(
				getArguments().getString(ARG_DEVICE));
	}

	@OnClick(R.id.addCustomButton)
	public void addCustomCommand() {
		mDialogHelper.showInputDialog(getString(R.string.dialog_command_name), "")
				.subscribe(name -> {
					mDialogHelper.showInputDialog(getString(R.string.dialog_custom_detail), "")
							.subscribe(action -> mRepo.addCustomCommand(new CustomCommand(name, action)));
				});
	}

	private void refreshCommands() {
		StaticUtils.observe(this, mService.getDevice(mDeviceName))
				.map(device -> {
					List<String> names = device.getCommands();
					Collections.sort(names, (a, b) -> StaticUtils.getCommandDisplayName(a)
							.compareToIgnoreCase(StaticUtils.getCommandDisplayName(b)));

					ArrayList<Command> commands = new ArrayList<>();
					for (String name : names) {
						commands.add(new Command(name, false));
					}
					return commands;
				})
				.subscribe(commands -> {
							mCommandsAdapter.setCommands(commands);
							mCommandsAdapter.notifyDataSetChanged();
							mCommandsList.expandGroup(CommandListAdapter.GROUP_COMMANDS);
						},
						error -> mDialogHelper.showErrorDialog(
								"Could not get commands for device " + mDeviceName));
	}

	public void executeCommand(Command command) {
		if (mService == null) {
			return;
		}

		showProgressDialog(getString(R.string.progress_command_title),
				getString(R.string.progress_command_instruction));

		StaticUtils.observe(this, mService.postCommand(mDeviceName, command))
				.subscribe(status -> {
							dismissProgressDialog();
						},
						error -> {
							dismissProgressDialog();
							mDialogHelper.showErrorDialog(getString(R.string.command_error));
						});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mRepo.save(mDeviceName);
	}
}
