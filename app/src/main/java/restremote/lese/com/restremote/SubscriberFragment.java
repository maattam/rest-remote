package restremote.lese.com.restremote;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

import rx.Observable;
import rx.Subscription;
import rx.android.app.AppObservable;

public class SubscriberFragment extends Fragment implements LifecycleSubscriber {
	private final ArrayList<Subscription> mSubscriptions = new ArrayList<>();

	private ProgressDialog mProgressDialog;
	protected DialogHelper mDialogHelper;

	public void addSubscription(Subscription subscription) {
		mSubscriptions.add(subscription);
	}

	public <T> Observable<T> bind(Observable<T> source) {
		return AppObservable.bindFragment(this, source);
	}

	protected void showProgressDialog(String title, String message) {
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
		mProgressDialog = ProgressDialog.show(getActivity(), title, message);
	}

	protected void dismissProgressDialog() {
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
			mProgressDialog = null;
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		((MyApplication)(getActivity().getApplication())).inject(this);

		mDialogHelper = new DialogHelper(getActivity());
		mDialogHelper.setLifecycle(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		for (Subscription subscription : mSubscriptions) {
			if (!subscription.isUnsubscribed()) {
				subscription.unsubscribe();
			}
		}

		dismissProgressDialog();
	}
}
