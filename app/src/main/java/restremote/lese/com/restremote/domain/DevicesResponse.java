package restremote.lese.com.restremote.domain;

import java.util.List;

public class DevicesResponse {
	private List<String> devices;

	public DevicesResponse(List<String> devices) {
		this.devices = devices;
	}

	public List<String> getDevices() {
		return devices;
	}
}
