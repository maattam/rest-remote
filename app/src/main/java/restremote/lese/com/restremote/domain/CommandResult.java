package restremote.lese.com.restremote.domain;

public class CommandResult {
	private boolean success;

	public CommandResult(boolean success) {
		this.success = success;
	}

	public boolean didSucceed() {
		return success;
	}
}
