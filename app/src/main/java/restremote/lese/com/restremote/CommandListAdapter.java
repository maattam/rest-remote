package restremote.lese.com.restremote;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import restremote.lese.com.restremote.domain.Command;
import restremote.lese.com.restremote.domain.CustomCommand;

public class CommandListAdapter extends BaseExpandableListAdapter {

	public static final int GROUP_COMMANDS = 0;
	public static final int GROUP_CUSTOM = 1;

	private final Context mContext;

	private final String[] mGroups;
	private List<Command> mCommands = new ArrayList<>();
	private List<CustomCommand> mCustomCommands = new ArrayList<>();

	public CommandListAdapter(Context context) {
		mContext = context;
		mGroups = new String[] {
				context.getString(R.string.list_group_command),
				context.getString(R.string.list_group_custom_command)
		};
	}

	public void setCommands(List<Command> commands) {
		mCommands = commands;
	}

	public void setCustomCommands(List<CustomCommand> commands) {
		mCustomCommands = commands;
	}

	public Command getCommand(int position) {
		return (Command) getChild(GROUP_COMMANDS, position);
	}

	public CustomCommand getCustomCommand(int position) {
		return (CustomCommand) getChild(GROUP_CUSTOM, position);
	}

	@Override
	public int getGroupCount() {
		return mGroups.length;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (groupPosition == GROUP_COMMANDS) {
			return mCommands.size();
		} else if (groupPosition == GROUP_CUSTOM) {
			return mCustomCommands.size();
		} else {
			return 0;
		}
	}

	@Override
	public Object getGroup(int groupPosition) {
		return mGroups[groupPosition];
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		if (groupPosition == GROUP_COMMANDS) {
			return mCommands.get(childPosition);
		} else if (groupPosition == GROUP_CUSTOM) {
			return mCustomCommands.get(childPosition);
		} else {
			return null;
		}
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return groupPosition + childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.list_group_command, parent, false);
		}

		TextView titleView = (TextView) convertView.findViewById(R.id.commandGroupName);
		titleView.setText((String) getGroup(groupPosition));

		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
		int id = groupPosition == GROUP_COMMANDS ? R.layout.list_item_command
				: R.layout.list_item_custom_command;

		convertView = LayoutInflater.from(mContext).inflate(id, parent, false);

		if (groupPosition == GROUP_COMMANDS) {
			TextView commandName = (TextView) convertView.findViewById(R.id.commandName);
			commandName.setText(StaticUtils.getCommandDisplayName(
					getCommand(childPosition).getCommand()));

		} else if (groupPosition == GROUP_CUSTOM) {
			TextView commandName = (TextView) convertView.findViewById(R.id.customCommandName);
			TextView commandDetail = (TextView) convertView.findViewById(R.id.customCommandDetail);

			CustomCommand command = getCustomCommand(childPosition);
			commandName.setText(command.getName());
			commandDetail.setText(command.getDetail());
		}

		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
