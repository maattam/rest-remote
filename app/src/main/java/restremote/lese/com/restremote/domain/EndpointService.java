package restremote.lese.com.restremote.domain;

public interface EndpointService {
	boolean isEndpointSet();

	String getEndpoint();

	void setEndpoint(String apiUrl);
}
