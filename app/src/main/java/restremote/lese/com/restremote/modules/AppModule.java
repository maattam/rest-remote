package restremote.lese.com.restremote.modules;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
		library = true,
		includes = DomainModule.class
)
public class AppModule {

	private final static String PREFS_FILE = "restremote_prefs";

	private final Application mApp;

	public AppModule(Application app) {
		mApp = app;
	}

	@Provides @Singleton
	public Context provideApplicationContext() {
		return mApp;
	}

	@Provides
	public SharedPreferences provideSharedPreferences() {
		return mApp.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
	}
}
