package restremote.lese.com.restremote.domain;

public class CustomCommand  {

	private String name;
	private String detail;

	public CustomCommand(String name, String detail) {
		this.name = name;
		this.detail = detail;
	}

	public static Command toCommand(CustomCommand custom) {
		return new Command(custom.detail, true);
	}

	public String getName() {
		return name;
	}

	public String getDetail() {
		return detail;
	}
}
