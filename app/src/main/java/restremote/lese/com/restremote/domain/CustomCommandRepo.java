package restremote.lese.com.restremote.domain;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.subjects.BehaviorSubject;

public class CustomCommandRepo implements DeviceRepo {

	private static final String PREF_COMMANDS = "custom_commands";

	private List<CustomCommand> mCommands = new ArrayList<>();

	private final BehaviorSubject<List<CustomCommand>> mCommandsSubject = BehaviorSubject.create();
	private final SharedPreferences mSharedPreferences;

	public CustomCommandRepo(SharedPreferences sharedPreferences) {
		mSharedPreferences = sharedPreferences;
		mCommandsSubject.onNext(mCommands);
	}

	public void load(String deviceName) {
		String json = mSharedPreferences.getString(PREF_COMMANDS + "_" + deviceName, null);
		if (json != null) {
			Type listType = new TypeToken<List<CustomCommand>>() {}.getType();
			mCommands = new Gson().fromJson(json, listType);
			mCommandsSubject.onNext(mCommands);
		}
	}

	public void save(String deviceName) {
		mSharedPreferences.edit().putString(PREF_COMMANDS + "_" + deviceName,
				new Gson().toJson(mCommands)).apply();
	}

	public Observable<List<CustomCommand>> getCustomCommands() {
		return mCommandsSubject;
	}

	public void addCustomCommand(CustomCommand command) {
		mCommands.add(command);
		mCommandsSubject.onNext(mCommands);
	}
}
