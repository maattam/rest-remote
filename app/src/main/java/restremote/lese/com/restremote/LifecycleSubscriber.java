package restremote.lese.com.restremote;

import rx.Observable;
import rx.Subscription;

public interface LifecycleSubscriber {
	void addSubscription(Subscription subscription);
	<T> Observable<T> bind(Observable<T> source);
}
