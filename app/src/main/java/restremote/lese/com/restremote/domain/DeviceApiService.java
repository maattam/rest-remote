package restremote.lese.com.restremote.domain;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

import rx.Observable;

public interface DeviceApiService {
	@GET("/devices")
	Observable<DevicesResponse> getDevices();

	@GET("/devices/{name}")
	Observable<Device> getDevice(@Path("name") String name);

	@POST("/devices/{name}/command")
	Observable<CommandResult> postCommand(@Path("name") String deviceName, @Body Command command);
}
