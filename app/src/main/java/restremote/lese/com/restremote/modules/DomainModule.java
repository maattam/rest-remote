package restremote.lese.com.restremote.modules;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import restremote.lese.com.restremote.DeviceFragment;
import restremote.lese.com.restremote.MainActivity;
import restremote.lese.com.restremote.NavigationDrawerFragment;
import restremote.lese.com.restremote.domain.CustomCommandRepo;
import restremote.lese.com.restremote.domain.DeviceEndpointService;
import restremote.lese.com.restremote.domain.DeviceApiService;
import restremote.lese.com.restremote.domain.DeviceRepo;
import restremote.lese.com.restremote.domain.EndpointService;
import retrofit.RestAdapter;

@Module(
		complete = false,
		library = true,
		injects = {
				MainActivity.class,
				DeviceFragment.class,
				NavigationDrawerFragment.class
		}
)
public class DomainModule {

	@Provides @Singleton
	public EndpointService provideEndpointService(SharedPreferences prefs) {
		return new DeviceEndpointService(prefs);
	}

	@Provides
	public DeviceApiService provideDeviceApiService(EndpointService endpoint) {
		if (!endpoint.isEndpointSet()) {
			return null;
		}

		RestAdapter adapter = new RestAdapter.Builder()
				.setEndpoint(endpoint.getEndpoint())
				.setLogLevel(RestAdapter.LogLevel.BASIC)
				.build();

		return adapter.create(DeviceApiService.class);
	}

	@Provides
	public DeviceRepo provideDeviceRepo(SharedPreferences prefs) {
		return new CustomCommandRepo(prefs);
	}
}
