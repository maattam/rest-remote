package restremote.lese.com.restremote;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import restremote.lese.com.restremote.domain.EndpointService;
import rx.Observable;
import rx.Subscription;
import rx.android.app.AppObservable;

public class MainActivity extends ActionBarActivity
		implements NavigationDrawerFragment.NavigationDrawerCallbacks, LifecycleSubscriber {

	/**
	 * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;
	private boolean mSectionAttached = false;
	private List<Subscription> mSubscriptions = new ArrayList<>();

	@InjectView(R.id.mainInstruction)
	protected TextView mInfoText;

	@Inject protected EndpointService mApiProvider;

	private DialogHelper mDialogHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ButterKnife.inject(this);
		((MyApplication)getApplication()).inject(this);

		mDialogHelper = new DialogHelper(this);
		mDialogHelper.setLifecycle(this);

		mNavigationDrawerFragment = (NavigationDrawerFragment)
				getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(
				R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));

		if (mSectionAttached) {
			mInfoText.setVisibility(View.INVISIBLE);
		}

		if (!mApiProvider.isEndpointSet()) {
			showHostnameDialog();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		for (Subscription subs : mSubscriptions) {
			if (!subs.isUnsubscribed()) {
				subs.unsubscribe();
			}
		}
	}

	@Override
	public void onNavigationDrawerItemSelected(String name) {
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.container, DeviceFragment.newInstance(name))
				.commit();
	}

	public void onSectionAttached(String name) {
		mTitle = name;
		setTitle(mTitle);
		mSectionAttached = true;

		if (mInfoText != null) {
			mInfoText.setVisibility(View.INVISIBLE);
		}
	}

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			showHostnameDialog();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void showHostnameDialog() {
		mDialogHelper.showInputDialog(getString(R.string.dialog_host_title), mApiProvider.getEndpoint())
				.subscribe(server -> {
					mApiProvider.setEndpoint(server);
					mNavigationDrawerFragment.refreshDevices();
				});
	}

	@Override
	public void addSubscription(Subscription subscription) {
		mSubscriptions.add(subscription);
	}

	@Override
	public <T> Observable<T> bind(Observable<T> source) {
		return AppObservable.bindActivity(this, source);
	}
}
