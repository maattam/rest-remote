package restremote.lese.com.restremote.domain;

import java.util.List;

public class Device {
	private String name;
	private List<String> commands;

	public Device(String name, List<String> commands) {
		this.name = name;
		this.commands = commands;
	}

	public String getName() {
		return name;
	}

	public List<String> getCommands() {
		return commands;
	}
}
