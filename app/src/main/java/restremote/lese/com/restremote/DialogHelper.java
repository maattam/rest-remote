package restremote.lese.com.restremote;

import android.app.AlertDialog;
import android.content.Context;
import android.widget.EditText;

import rx.Observable;

public class DialogHelper {

	private final Context mContext;
	private LifecycleSubscriber mLifecycle;

	public DialogHelper(Context context) {
		mContext = context;
	}

	public void setLifecycle(LifecycleSubscriber lifecycle) {
		mLifecycle = lifecycle;
	}

	public void showErrorDialog(String message) {
		new AlertDialog.Builder(mContext)
				.setTitle(null)
				.setMessage(message)
				.setPositiveButton("Ok", (dialog, which) -> dialog.dismiss())
				.create().show();
	}

	public Observable<String> showInputDialog(String title, String defaultText) {
		if (mLifecycle != null) {
			return StaticUtils.observe(mLifecycle, createInputDialog(title, defaultText));
		}

		return Observable.create(createInputDialog(title, defaultText)::subscribe);
	}

	private Observable<String> createInputDialog(String title, String defaultText) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setTitle(title);

		final EditText edit = new EditText(mContext);
		edit.setText(defaultText);
		builder.setView(edit);

		return Observable.create(subscriber -> {
				builder.setPositiveButton(R.string.dialog_ok, (dialog, which) -> {
					dialog.dismiss();

					subscriber.onNext(edit.getText().toString());
					subscriber.onCompleted();
				});

				builder.setNegativeButton(R.string.dialog_cancel, (dialog, which) -> {
					dialog.dismiss();
					subscriber.onCompleted();
				});

				builder.create().show();
			});
	}
}
