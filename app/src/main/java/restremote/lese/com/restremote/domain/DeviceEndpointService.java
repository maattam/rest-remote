package restremote.lese.com.restremote.domain;

import android.content.SharedPreferences;

public class DeviceEndpointService implements EndpointService {

	private static final String PREFS_API_KEY = "api_url";

	private final SharedPreferences mPreferences;

	public DeviceEndpointService(SharedPreferences sharedPreferences) {
		mPreferences = sharedPreferences;
	}

	public boolean isEndpointSet() {
		String apiUrl = getEndpoint();
		return apiUrl != null && !apiUrl.isEmpty();
	}

	public void setEndpoint(String apiUrl) {
		mPreferences.edit().putString(PREFS_API_KEY, apiUrl).apply();
	}

	public String getEndpoint() {
		return mPreferences.getString(PREFS_API_KEY, null);
	}
}
