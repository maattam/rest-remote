package restremote.lese.com.restremote;

import rx.Observable;

public class StaticUtils {

	public static String getCommandDisplayName(String command) {
		switch (command) {
			case "standby": 			return "Standby";
			case "deactivate-source": 	return "Set as inactive input";
			case "power-on": 			return "Turn on";
			case "activate-source": 	return "Set as active input";
		}
		return command;
	}

	public static <T> Observable<T> observe(LifecycleSubscriber lifecycle, Observable<T> source) {
		return Observable.create(subscriber -> {
			lifecycle.addSubscription(lifecycle.bind(source).subscribe(subscriber));
		});
	}
}
