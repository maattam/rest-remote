package restremote.lese.com.restremote.domain;

import java.util.List;

import rx.Observable;

public interface DeviceRepo {
	void load(String tag);

	void save(String tag);

	Observable<List<CustomCommand>> getCustomCommands();

	void addCustomCommand(CustomCommand command);
}
