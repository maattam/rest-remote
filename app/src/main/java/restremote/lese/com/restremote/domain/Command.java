package restremote.lese.com.restremote.domain;

public class Command {
	private String command;
	private boolean isRaw;

	public Command(String command, boolean isRaw) {
		this.command = command;
		this.isRaw = isRaw;
	}

	public String getCommand() {
		return command;
	}
}
