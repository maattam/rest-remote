package restremote.lese.com.restremote;

import android.app.Application;
import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.List;

import dagger.ObjectGraph;
import restremote.lese.com.restremote.modules.AppModule;

public class MyApplication extends Application {

	private ObjectGraph mObjectGraph;

	@Override
	public void onCreate() {
		super.onCreate();
		mObjectGraph = ObjectGraph.create(getModules().toArray());
	}

	private List<Object> getModules() {
		return Arrays.asList(new AppModule(this));
	}

	public <T> void inject(@NonNull T object) {
		mObjectGraph.inject(object);
	}
}
